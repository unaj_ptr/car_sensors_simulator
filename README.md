## Simulación de Robot para Programación en Tiempo Real

Aplicación que simula un robot con sensores, y pide a otro recurso que calcule la ruta de un punto a otro.

La simulación permite armar escenarios (mapas).

## Pasos para ejecución

### Requerimientos

* Python >=3
* pygame

### Instalar librerias

	`~$ pip install -r requirements.txt`

### Correr simulador

	`~$ python app.py

