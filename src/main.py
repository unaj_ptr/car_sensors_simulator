import sys
from time import time

import pygame
from pygame import Color, Rect

from .chart import Chart
from .constants import LOG_HEIGHT, MARGIN_LEFT, MARGIN_TOP, MAP_WIDTH, \
	MAP_HEIGHT, OTHER_WIN_WIDTH, OTHER_WIN_HEIGHT, SCREEN_HEIGHT,  \
	SCREEN_WIDTH, STEP_SIZE
from .routefinder import Route


class MainWindow(object):
    """ Clase principal. Crea la ventana de visualización."""
    route = []
    #color de fondo de mapa
    background_color = Color('white')
    # Color de lineas de matriz
    line_matrix_color = Color('black')
    # Color de punto de largada
    start_pos_color = Color('green')
    # Color de punto de llegada
    end_pos_color = Color('red')
    # Color de la ruta calculada
    route_color = Color('gray') 
    # Color de obstaculos
    blocked_color = Color('black')
    
    def __init__(self, screen, field, message_func):
        """ Constructor de la ventana.
        params:
            screen
            field
            message_func
        """
        self.screen = screen
        self.field = field
        self.message_func = message_func

        self._create_map()
        
    def draw(self):
        self._draw_grid(self.field)
        self._draw_map(self.field, 
            self.blocked_list, self.start_pos,
            self.goal_pos, self.route)
        
        self.message_func(self.msg1, self.msg2)
    
    def user_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_F5:
                self._recompute_route()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            self.route_valid = False
            self.msg1 = 'F5 para calcular'
            self.msg2 = ''
            
            self._handle_mouse_click(event)
    
    def _create_map(self):
        """ """
        self.start_pos = 0, 0
        self.goal_pos = 3, 8
        
        nrows = self.field.height // STEP_SIZE
        ncols = self.field.width // STEP_SIZE
        
        self.map = Chart(nrows, ncols)
        for b in [ (1, 1), (1, 2), (0, 3), (1, 3), (2, 3), 
                    (2, 4), (2, 5), (2, 6)]:
            self.map.set_blocked(b)
                    
        self._recompute_route()
    
    def _handle_mouse_click(self, event):
        if not self.field.collidepoint(event.pos):
            return
        
        ncol = (event.pos[0] - self.field.left) // STEP_SIZE
        nrow = (event.pos[1] - self.field.top) // STEP_SIZE
        coord = (nrow, ncol)

        if event.button == 1:
            self.map.set_blocked(coord, not self.map.blocked[coord])
        elif event.button == 2:
            self.start_pos = coord
        elif event.button == 3:
            self.goal_pos = coord
        
    def _recompute_route(self):
        
        self.blocked_list = self.map.blocked
        
        route_finder = Route(self.map.successors)
        t = time()
        self.route = route_finder.get_route(self.start_pos, self.goal_pos)
        dt = time() - t

        if self.route == []:
            self.msg1 = "No se encontro ruta."
        else:
            self.msg1 = "Ruta encontrada. Total a recorrer {} saltos".format(len(self.route))
            
        self.msg2 = "Tiempo: {:.4f} seconds".format(dt)
        self.route_valid = True

    def _draw_grid(self, field):
        """ """
        self.screen.fill(self.background_color, field)
        
        nrows = field.height // STEP_SIZE
        ncols = field.width // STEP_SIZE
        
        for y in range(nrows + 1):
            pygame.draw.line(
                self.screen,
                self.line_matrix_color,
                (field.left, field.top + y * STEP_SIZE - 1),
                (field.right - 1, field.top + y * STEP_SIZE - 1))
        
        for x in range(ncols + 1):
            pygame.draw.line(
                self.screen,
                self.line_matrix_color,
                (field.left + x * STEP_SIZE - 1, field.top),
                (field.left + x * STEP_SIZE - 1, field.bottom - 1))

    def _draw_map(self, field, blocked, start, goal, route):
        
        def _fill_spot(nrow_ncol, color):
            pos_x = field.left + nrow_ncol[1] * STEP_SIZE + STEP_SIZE // 2
            pos_y = field.top + nrow_ncol[0] * STEP_SIZE + STEP_SIZE // 2
            radius = STEP_SIZE // 4
            pygame.draw.circle(self.screen, 
                color, (pos_x, pos_y), radius)
            # TODO cambiar punto de llegada y partida por bandera
            # ~ import os
            # ~ current_dir = os.path.dirname(os.path.abspath(__file__))
            # ~ image_path = os.path.join(current_dir, "images/car.png")
            # ~ car_image = pygame.image.load(image_path).convert()
            # ~ self.screen.blit(car_image, (pos_x, pos_y))

        for bl in blocked:
            left = field.left + bl[1] * STEP_SIZE 
            top = field.top + bl[0] * STEP_SIZE
            width = STEP_SIZE - 1

            self.screen.fill(self.blocked_color, Rect(left, top, width, width))

        
        if self.route_valid:
            for route_square in route:
                _fill_spot(route_square, self.route_color)

        _fill_spot(start, self.start_pos_color)
        _fill_spot(goal, self.end_pos_color)
    

def draw_messages(screen, rect, message1, message2):
    draw_rimmed_box(screen, rect, Color('black'), 1, Color('white'))
    
    my_font = pygame.font.SysFont('courier new', 14, bold=False)
    message1_sf = my_font.render(message1, True, Color('white'))
    message2_sf = my_font.render(message2, True, Color('white'))
    
    screen.blit(message1_sf, rect.move(10, 0))
    screen.blit(message2_sf, rect.move(10, message1_sf.get_height()))


def draw_rimmed_box(screen, box_rect, box_color, 
                    rim_width=0, 
                    rim_color=Color('black')):
    """ Draw a rimmed box on the given surface. The rim is drawn
        outside the box rect.
    """
    if rim_width:
        rim_rect = Rect(box_rect.left - rim_width,
                        box_rect.top - rim_width,
                        box_rect.width + rim_width * 2,
                        box_rect.height + rim_width * 2)
        pygame.draw.rect(screen, rim_color, rim_rect)
    
    pygame.draw.rect(screen, box_color, box_rect)


def draw_title(screen, rect):
    draw_rimmed_box(screen, rect, Color('black'), 1, Color('white'))
    
    msgs = ['#TODO BOTONES!!!!']
    
    my_font = pygame.font.SysFont('courier new', 14, bold=False)
    
    for i, msg in enumerate(msgs):
        rendered = my_font.render(msg, True, Color('white'))
        screen.blit(rendered, rect.move(10, i * rendered.get_height()))
    

def run_robot():
    """ """
    # Crear zonas: Rect(left, top, width, height)
	# matriz para recorrer
    FIELD_RECT = Rect(MARGIN_LEFT, MARGIN_TOP, MAP_WIDTH, MAP_HEIGHT)

    # Pantalla de acciones
    TITLE_RECT = Rect(MARGIN_LEFT * 2 + MAP_WIDTH, MARGIN_TOP, OTHER_WIN_WIDTH, OTHER_WIN_HEIGHT)

    # Pantalla de log
    MESSAGES_RECT = Rect(MARGIN_LEFT * 2 + MAP_WIDTH, OTHER_WIN_HEIGHT + MARGIN_TOP * 2, OTHER_WIN_WIDTH , LOG_HEIGHT)
    
    pygame.init()
    screen = pygame.display.set_mode(
                (SCREEN_WIDTH, SCREEN_HEIGHT), 0, 32)
    clock = pygame.time.Clock()
    
    def message_func(msg1, msg2):
        draw_messages(screen, MESSAGES_RECT, msg1, msg2)
    
    main_window = MainWindow(screen, FIELD_RECT, message_func)
    
    while True:
        time_passed = clock.tick(30)
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit_game()
            else:
                main_window.user_event(event)
   
        draw_title(screen, TITLE_RECT)
    
        main_window.draw()
       
        pygame.display.flip()


def exit_game():
    sys.exit()
