# Tamaño de la cuadrícula (cada cuadrado - paso)
STEP_SIZE = 20

MATRIX_RANGE = 35
MARGIN_LEFT = 25
MARGIN_TOP = 15
OTHER_WIN_WIDTH = 500
OTHER_WIN_HEIGHT = 300

# Dimensiones de matriz para recorrer    
MAP_WIDTH = MATRIX_RANGE * STEP_SIZE
MAP_HEIGHT = MAP_WIDTH

# Dimensiones de la pantalla completa
SCREEN_WIDTH = MARGIN_LEFT * 3 + MAP_WIDTH + OTHER_WIN_WIDTH
SCREEN_HEIGHT = MARGIN_TOP * 2 + MAP_HEIGHT

# Dimensiones de la pantalla de log
LOG_HEIGHT = SCREEN_HEIGHT - OTHER_WIN_HEIGHT - MARGIN_TOP * 3
   
